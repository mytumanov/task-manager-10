package ru.mtumanov.tm.service;

import java.util.List;

import ru.mtumanov.tm.api.IProjectRepository;
import ru.mtumanov.tm.api.IProjectService;
import ru.mtumanov.tm.model.Project;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }
    
    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void add (final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}

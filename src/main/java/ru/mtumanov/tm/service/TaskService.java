package ru.mtumanov.tm.service;

import java.util.List;

import ru.mtumanov.tm.api.ITaskRepository;
import ru.mtumanov.tm.api.ITaskService;
import ru.mtumanov.tm.model.Task;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.add(task);        
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }
    
}

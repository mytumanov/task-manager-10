package ru.mtumanov.tm.service;

import ru.mtumanov.tm.api.ICommandRepository;
import ru.mtumanov.tm.api.ICommandService;
import ru.mtumanov.tm.model.Command;

public class CommandService implements ICommandService {
    
    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}

package ru.mtumanov.tm.api;

import java.util.List;

import ru.mtumanov.tm.model.Project;

public interface IProjectRepository {

    public void add (final Project project);

    public List<Project> findAll();

    public void clear();
    
}

package ru.mtumanov.tm.api;

import ru.mtumanov.tm.model.Project;

public interface IProjectService extends IProjectRepository {
    
    public Project create(final String name, final String description);
    
}

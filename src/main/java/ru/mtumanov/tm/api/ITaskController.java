package ru.mtumanov.tm.api;

public interface ITaskController {
    
    public void createTask();

    public void showTasks();

    public void clearTasks();

}

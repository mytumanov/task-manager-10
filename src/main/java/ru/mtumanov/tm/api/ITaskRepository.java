package ru.mtumanov.tm.api;

import java.util.List;

import ru.mtumanov.tm.model.Task;

public interface ITaskRepository {
    
    public void add (final Task task);

    public List<Task> findAll();

    public void clear();

}

package ru.mtumanov.tm.api;

import ru.mtumanov.tm.model.Command;

public interface ICommandRepository {
    
    Command[] getTerminalCommands();
    
}

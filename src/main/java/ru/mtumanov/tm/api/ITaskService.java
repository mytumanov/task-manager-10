package ru.mtumanov.tm.api;

import ru.mtumanov.tm.model.Task;

public interface ITaskService extends ITaskRepository {
    
    public Task create(final String name, final String description);

}

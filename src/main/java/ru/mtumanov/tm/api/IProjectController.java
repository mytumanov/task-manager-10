package ru.mtumanov.tm.api;

public interface IProjectController {

    public void createProject();

    public void showProjects();

    public void clearProjects();
    
}

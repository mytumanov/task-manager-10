package ru.mtumanov.tm.component;

import ru.mtumanov.tm.api.ICommandController;
import ru.mtumanov.tm.api.ICommandRepository;
import ru.mtumanov.tm.api.ICommandService;
import ru.mtumanov.tm.api.IProjectController;
import ru.mtumanov.tm.api.IProjectRepository;
import ru.mtumanov.tm.api.IProjectService;
import ru.mtumanov.tm.api.ITaskController;
import ru.mtumanov.tm.api.ITaskRepository;
import ru.mtumanov.tm.api.ITaskService;
import ru.mtumanov.tm.constant.ArgumentConstant;
import ru.mtumanov.tm.constant.CommandConstant;
import ru.mtumanov.tm.controller.CommandController;
import ru.mtumanov.tm.controller.ProjectController;
import ru.mtumanov.tm.controller.TaskController;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.service.CommandService;
import ru.mtumanov.tm.service.ProjectService;
import ru.mtumanov.tm.service.TaskService;
import ru.mtumanov.tm.util.TerminalUtil;

public final class Bootstrap {
    
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void start(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        while (true) {
            System.out.println("ENTER COMMAND:");
            processCommand(TerminalUtil.nextLine());
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.CMD_ABOUT:
                commandController.showAbout();
            break;
            case ArgumentConstant.CMD_HELP:
                commandController.showHelp();
            break;
            case ArgumentConstant.CMD_VERSION:
                commandController.showVersion();
            break;
            case ArgumentConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArguemntError();
            break;
        }
    }

    private void processCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConstant.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConstant.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConstant.CMD_EXIT:
                commandController.exit();
                break;
            case CommandConstant.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
             case CommandConstant.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
             case CommandConstant.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

}

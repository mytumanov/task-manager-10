package ru.mtumanov.tm.repository;

import java.util.ArrayList;
import java.util.List;

import ru.mtumanov.tm.api.ITaskRepository;
import ru.mtumanov.tm.model.Task;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tasks.add(task);    
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }
    
}

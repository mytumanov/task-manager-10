package ru.mtumanov.tm.repository;

import java.util.ArrayList;
import java.util.List;

import ru.mtumanov.tm.api.IProjectRepository;
import ru.mtumanov.tm.model.Project;

public final class ProjectRepository implements IProjectRepository {
    
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
